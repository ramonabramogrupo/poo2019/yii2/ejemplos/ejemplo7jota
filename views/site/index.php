<?php
use yii\helpers\Html;

$this->title = 'My Yii Application';

/* cargar un css */
$this->registerCssFile(
        '@web/css/mio.css', 
        ['depends' => [yii\bootstrap\BootstrapAsset::className()]
        ]);

/* cargar un js */
$this->registerJsFile('@web/js/mio.js', 
        ['depends'=> [yii\web\JqueryAsset::className()]
        ]);

?>

<div>
    <h1><?= $titulo ?></h1>
</div>
<div>
    <?= Html::img("@web/imgs/" . $foto ,[
        "class"=>"foto1"
    ]) ?>
</div>
<div>
    <div><?= $texto ?></div>
</div>